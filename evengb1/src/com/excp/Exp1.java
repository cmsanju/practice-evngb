package com.excp;

public class Exp1 {
	
	public static void main(String[] args) {
		
		try
		{
			System.out.println("test");
			
			System.out.println(23/2);
			
			int[] ar = {12,34,56};
			
			System.out.println(ar[2]);
			
			String str = "java";
			
			System.out.println(str.charAt(2));
			
			String st = "hello";
			
			System.out.println(st.charAt(1));
			
			String val = "aba";
			
			int x = Integer.parseInt(val);
		}
		catch(ArithmeticException ae)
		{
			System.out.println("can't divided by zero");
		}
		catch(ArrayIndexOutOfBoundsException aie)
		{
			System.out.println("check array size");
		}
		catch(StringIndexOutOfBoundsException sie)
		{
			System.out.println("check string length");
		}
		catch(NullPointerException npe)
		{
			System.out.println("enter value for input string");
		}
		catch(Exception e)
		{
			System.out.println("check your inputs");
		}
		finally
		{
			System.out.println("i am from finally");
		}
	}

}
