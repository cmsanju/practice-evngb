package com.excp;

public class Exp2 {
	
	public static void main(String[] args) {
		try
		{
			System.out.println(38/0);
		}
		catch(Exception e)
		{
			//1 getMessage will give only message
			System.out.println(e.getMessage());
			
			//2 printing exception class object
			System.out.println(e);
			
			//3 using printStackTrace();
			e.printStackTrace();
		}
		
	}

}
