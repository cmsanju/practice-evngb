package com.fls;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class DeserialDemo {
	
	public static void main(String[] args) throws Exception
	{
		
		FileInputStream fis = new FileInputStream("src/student.txt");
		
		ObjectInputStream robj = new ObjectInputStream(fis);
		
		Student sobj = (Student)robj.readObject();
		
		System.out.println(sobj.id+" "+sobj.name+" "+sobj.city+" "+sobj.pin);
	}

}
