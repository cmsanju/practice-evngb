package com.fls;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class SerialDemo {
	
	public static void main(String[] args) throws Exception
	{
		
		FileOutputStream fs = new FileOutputStream("src/student.txt");
		
		ObjectOutputStream obj = new ObjectOutputStream(fs);
		
		Student sobj = new Student();
		
		sobj.id = 101;
		sobj.name = "Hero";
		sobj.city = "Blr";
		sobj.pin = 124144;
		
		obj.writeObject(sobj);
		
		System.out.println("Done.");
	}

}
