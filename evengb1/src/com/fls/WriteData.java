package com.fls;

import java.io.File;
import java.io.FileOutputStream;

public class WriteData {
	
	public static void main(String[] args) throws Exception
	{
		
		File file = new File("src/write.txt");
		
		FileOutputStream fw = new FileOutputStream(file);
		
		String msg = "This is byte stream file write and read operations";
		
		fw.write(msg.getBytes());
		
		System.out.println("Done.");
	}

}
