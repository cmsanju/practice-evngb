package com.test;

public class Exp9 {
	
	public static void main(String[] args) {
		
		int[] ar = new int[5];
		
		ar[0] = 22;
		
		ar[1] = 33;
		ar[2] = 44;
		
		ar[4]  = 55;
		
		System.out.println(ar[5]);
		
		int[] ar1 = {10,7,89,66,47};
		
		System.out.println(ar1[0]);
		System.out.println(ar1[1]);
		System.out.println(ar1[2]);
		
		
		for(int i = 0; i<ar1.length;i++)
		{
			System.out.println(ar1[i]);
		}
		
		for(int x : ar1)
		{
			System.out.println(x);
		}
		
		
	}

}
