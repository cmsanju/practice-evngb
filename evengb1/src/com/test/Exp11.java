package com.test;

class Employee
{
	public int id;
	public String name;
	
	public Employee(int id, String name)
	{
		this.id = id;
		this.name = name;
	}
}

public class Exp11 {
	
	public static void main(String[] args) {
		
		Employee[] data = new Employee[5];
		
		data[0] = new Employee(11, "Java");
		
		data[1] = new Employee(22, "Spring");
		
		System.out.println(data[0].id+" "+data[0].name);
		System.out.println(data[1].id+" "+data[1].name);
	}

}
