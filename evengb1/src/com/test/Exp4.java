package com.test;

import java.util.Scanner;

class Student
{
	private int id;
	
	private String name;
	
	private String city;
	
	
	public void setId(int id)
	{
		this.id = id;
	}
	
	public int getId()
	{
		return this.id;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getName()
	{
		return this.name;
	}
	
	public void setCity(String city)
	{
		this.city = city;
	}
	
	public String getCity()
	{
		return this.city;
	}
	
}


public class Exp4 {
	
	public static void main(String[] args) {
		
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("enter id");
		
		int id = sc.nextInt();
		
		System.out.println("enter name");
		
		String name = sc.next();
		
		System.out.println("enter city");
		
		String city = sc.next();
		
		Student std = new Student();
		
		std.setId(id);
		std.setName(name);
		std.setCity(city);
		
		System.out.println("ID : "+std.getId()+" Name : "+std.getName()+" City : "+std.getCity());
		
	}

}
