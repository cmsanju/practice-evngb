package com.test;

public class Exp12 {
	
	public static void main(String[] args) {
		
		String str1 = "java";
		String str2 = "hello";
		String str3 = "java";
		
		String str4 = new String("java");
		String str5 = new String("hello");
		
		
		System.out.println(str1 == str3);
		
		System.out.println(str1 == str4);
		
		
		System.out.println(str1.equals(str4));
		
		
		String x = "eveng";
		
		System.out.println(x.hashCode());
		
		x = "eveng batch";
		
		System.out.println(x.hashCode());
		
		String y = "java";
		
		String z = " 1.8";
		
		y = y+z;
		
		System.out.println(y);	
	}

}
