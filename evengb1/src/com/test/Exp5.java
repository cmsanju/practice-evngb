package com.test;

public class Exp5 {
	
	static final int x = 40;// class level data
	
	     int y = 9;//object level data
	     
	     public void disp()
	     {
	    	 System.out.println(y);
	    	 
	    	 System.out.println(Exp5.x);
	     }
	     
	     private static void show()
	     {
	    	 System.out.println(x);
	    	 
	    	 Exp5 x = new Exp5();
	    	 
	    	 System.out.println(x.y);
	     }
	     
	     public final static void main(String[] args) {
			
	    	 Exp5.show();
	    	 
	    	 Exp5 obj = new Exp5();
	    	 
	    	 obj.disp();
	    	 
	    	 System.out.println(70*80);
		}

}
