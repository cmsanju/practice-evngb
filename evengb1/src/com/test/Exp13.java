package com.test;

public class Exp13 {
	
	public static void main(String[] args) {
		
		String str1 = new String("java");
		
		String str2 = "hello";
		
		StringBuffer sb = new StringBuffer(str1);
		
		sb.append(" 1.8");
		
		System.out.println(sb);
		
		StringBuffer sb1 = new StringBuffer(str2);
		
		System.out.println(sb1.reverse());
		
		String str3 = "Hi ";
		
		str3.concat("Hello");
		
		System.out.println(str3);
	}

}
