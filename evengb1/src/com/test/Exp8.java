package com.test;

public class Exp8 {
	
	public static void show()
	{
		System.out.println("static method");
	}
	
	public void disp()
	{
		System.out.println("non static method");
	}
	
	static
	{
		System.out.println("static");
	}
	
	{
		System.out.println("instance");
	}
	
	public static void main(String[] args) {
		
		System.out.println("Main method");
		
		Exp8 e = new Exp8();
		
		show();
		
		e.disp();
		
		Exp8  e1 = new Exp8();
	}

}
