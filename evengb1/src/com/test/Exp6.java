package com.test;

public class Exp6 {
	
	//1 method taking the arguments and returning the value
	
	public int method1(int x, int y)
	{
		System.out.println("method 1");
		
		return x;
		
	}
	
	//2 method not taking the arguments and not returning
	
	public void method2() {
		
		System.out.println("method 2");
	}
	
	//3 method taking the arguments but not returning
	
	public void method3(String str)
	{
		System.out.println("method 3");
	}
	
	//4 method not taking the arguments but returning 
	
	public String method4()
	{
		System.out.println("method 4");
		
		return "hello";
	}
	
	public static void main(String[] args) {
		
		Exp6 e = new Exp6();
		
		e.method1(23, 90);
		e.method2();
		e.method3("java");
		e.method4();
	}

}
