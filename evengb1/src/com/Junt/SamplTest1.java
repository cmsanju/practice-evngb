package com.Junt;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class SamplTest1 {
	
Sample obj;

static String msg = null;
	
	@BeforeAll
	public static void beforeClass()
	{
		System.out.println("Before class");
		
		msg = "Hi this junit test";
	}
	
	@AfterAll
	public static void afterClass()
	{
		System.out.println("After class");
		
		msg = null;
	}
	
	@BeforeEach
	public void setUp()
	{
		System.out.println("before test method");
		
		obj = new Sample();
	}
	
	@AfterEach
	public void setDown()
	{
		System.out.println("after test method");
		
		obj = null;
	}
	
	@Test
	public void testAdd()
	{
		System.out.println("test add method");
		
		int x = obj.add(30, 30);
		
		assertEquals(60, x);
	}
	
	@Test
	public void testSub()
	{
		System.out.println("test sub method");
		
		assertEquals(20, obj.sub(50, 30));
	}
	
	@Test
	public void testMul()
	{
		System.out.println("test mul method");
		
		assertEquals(100, obj.mul(10, 10));
	}
	
	@Test
	public void testGreetMsg()
	{
		System.out.println("test greet msg method");
		
		assertEquals("hi hello", obj.greetMsg(msg));
	}

}
