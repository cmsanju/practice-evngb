package com.Junt;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class SampleTest {
	
	Sample obj;
	
	@BeforeClass
	public static void beforeClass()
	{
		System.out.println("Before class");
	}
	
	@AfterClass
	public static void afterClass()
	{
		System.out.println("After class");
	}
	
	@Before
	public void setUp()
	{
		System.out.println("before test method");
	}
	
	@After
	//@Category
	public void setDown()
	{
		System.out.println("after test method");
	}
	
	@Test
	public void testAdd()
	{
		System.out.println("test add method");
	}
	
	@Test
	public void testSub()
	{
		System.out.println("test sub method");
	}
	
	@Test
	public void testMul()
	{
		System.out.println("test mul method");
	}
	
	@Test
	public void testGreetMsg()
	{
		System.out.println("test greet msg method");
	}

}
