package com.oops;

interface Inf3
{
	int x = 112;
	void pet();
	void details();
}

interface Inf4
{
	int x = 145;
	
	void pet();
	void animal();
}

class Impl1 implements Inf3,Inf4
{

	@Override
	public void pet() {
		// TODO Auto-generated method stub
		
		System.out.println(Inf3.x+" "+Inf4.x);
	}

	@Override
	public void animal() {
		// TODO Auto-generated method stub
		
	}

	

	@Override
	public void details() {
		// TODO Auto-generated method stub
		
	}
	
}

public class Exp4 {
	
	public static void main(String[] args) {
		
		Impl1 ojb = new Impl1();
		
		Inf4 obj1 = new Impl1();
		
		obj1.pet();
		
		Inf3 obj2 = new Impl1();
		
		obj2.pet();
	}

}
