package com.oops;

interface SMSService
{
	void greet();
}

class Child3 implements SMSService
{

	@Override
	public void greet() {
		
		System.out.println("Hi this is child3");
	}
	
}

class Child4 implements SMSService
{

	@Override
	public void greet() {
		System.out.println("Hi this is child4");
	}
	
}

public class Exp10 {
	
	public static void main(String[] args) {
		
		SMSService obj1 = new Child3();
		
		obj1.greet();
		
		SMSService obj2 = new Child4();
		
		obj2.greet();
		
		
	}

}
