package com.oops;

interface Inf1
{
	void books();
	
	default void show()
	{
		System.out.println("default method");
	}
	
	static void pet()
	{
		System.out.println("static method");
	}
}

abstract class Abs
{
	private void hiHello()
	{
		
	}
	public abstract void animal();
	
	public void disp()
	{
		System.out.println("abs default method");
		
		hiHello();
	}
}

class Impl extends Abs implements Inf1
{

	@Override
	public void animal() {
		
		System.out.println("abs overrided");
	}

	@Override
	public void books() {
		
		System.out.println("inf1 overrided");
	}
	
	
}

public class Exp3 {
	
	public static void main(String[] args) {
		
		
		Impl obj = new Impl();
		
		obj.animal();
		obj.books();
		obj.disp();
		obj.show();
		
		Inf1.pet();
		
	}

}
