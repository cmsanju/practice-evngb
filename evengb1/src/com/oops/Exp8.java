package com.oops;

import java.util.Scanner;

abstract class StudentInfo
{
	public abstract void studentDetails(Student obj);
	
}

class StdImpl extends StudentInfo
{

	@Override
	public void studentDetails(Student obj) {
		
		if(obj.getMarks() > 60 && obj.getMarks() < 70)
		{
			System.out.println("ID : "+obj.getId()+" Name : "+obj.getName()+" College : "+obj.getClg()+" City : "+obj.getCity()+" Marks : "+obj.getMarks());
			System.out.println("eligible for basic branches");
		}
		else if(obj.getMarks() >=70 && obj.getMarks() < 80)
		{
			System.out.println("ID : "+obj.getId()+" Name : "+obj.getName()+" College : "+obj.getClg()+" City : "+obj.getCity()+" Marks : "+obj.getMarks());
			System.out.println("B grade branches");
		}
		else if(obj.getMarks() >= 80)
		{
			System.out.println("ID : "+obj.getId()+" Name : "+obj.getName()+" College : "+obj.getClg()+" City : "+obj.getCity()+" Marks : "+obj.getMarks());
			System.out.println("A grade brancehs");
		}
		else
		{
			System.out.println("ID : "+obj.getId()+" Name : "+obj.getName()+" College : "+obj.getClg()+" City : "+obj.getCity()+" Marks : "+obj.getMarks());
			System.out.println("not eligible for any branch");
		}
		
	}
	
}

public class Exp8 {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter id ");
		
		int id = sc.nextInt();
		
		System.out.println("enter name");
		
		String name = sc.next();
		
		System.out.println("enter college");
		
		String clg = sc.next();
		
		System.out.println("enter city");
		
		String city = sc.next();
		
		System.out.println("enter marks");
		
		int marks = sc.nextInt();
		
		Student obj = new Student();
		
		obj.setId(id);
		obj.setName(name);
		obj.setClg(clg);
		obj.setCity(city);
		obj.setMarks(marks);
		
		StudentInfo sinf = new StdImpl();
		
		sinf.studentDetails(obj);
	}

}
