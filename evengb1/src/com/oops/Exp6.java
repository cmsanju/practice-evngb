package com.oops;

@FunctionalInterface
interface FunInf
{
	void greet();
	
	
}
public class Exp6 {
	
	public static void main(String[] args) {
		
		FunInf obj = new FunInf()
				{
					@Override
					public void greet()
					{
						System.out.println("overrided");
					}
				};
				
		new FunInf()
		{
			@Override
			public void greet()
			{
				System.out.println("nameless object");
			}
		}.greet();
		
		//jdk 8 onwards
		
		FunInf obj1 = () -> {
			System.out.println("lambda expression");
		};
		
		obj1.greet();
		
	}

}
//java.lang.Cloneable, java.io.Serializable, java.rmi.Remote etc.
