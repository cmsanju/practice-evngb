package com.oops;

import java.util.Scanner;

class Student1
{
	private int id;
	
	private String name;
	
	private int marks;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMarks() {
		return marks;
	}

	public void setMarks(int marks) {
		this.marks = marks;
	}
}

public class Exp7 {
	
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("enter id");
		
		int id = sc.nextInt();
		
		System.out.println("enter name");
		
		String name = sc.next();
		
		System.out.println("enter marks");
		
		int marks = sc.nextInt();
		
		Student1 obj = new Student1();
		
		obj.setId(id);
		obj.setName(name);
		obj.setMarks(marks);
		
		if(obj.getMarks() > 60 && obj.getMarks() < 70)
		{
			System.out.println("ID : "+obj.getId()+" Name : "+obj.getName()+" Marks : "+obj.getMarks());
			System.out.println("eligible for basic branches");
		}
		else if(obj.getMarks() >=70 && obj.getMarks() < 80)
		{
			System.out.println("ID : "+obj.getId()+" Name : "+obj.getName()+" Marks : "+obj.getMarks());
			System.out.println("B grade branches");
		}
		else if(obj.getMarks() >= 80)
		{
			System.out.println("ID : "+obj.getId()+" Name : "+obj.getName()+" Marks : "+obj.getMarks());
			System.out.println("A grade brancehs");
		}
		else
		{
			System.out.println("ID : "+obj.getId()+" Name : "+obj.getName()+" Marks : "+obj.getMarks());
			System.out.println("not eligible for any branch");
		}
		
		
	}

}
