package com.oops;


 class Parent
{
	public  void draw()
	{
		System.out.println("rle");
	}
}

class Child1 extends Parent
{
	@Override
	public void draw()
	{
		System.out.println("cle");
	}
}

class Child2 extends Parent
{
	@Override
	public void draw()
	{
		System.out.println("tle");
	}
}

public class Exp9 {
	
	public static void main(String[] args) {
		
		Parent obj = new Child1();//dynamic binding 
		
		obj.draw();
		
		Parent obj1 = new Child2();
		
		obj1.draw();
	}

}
