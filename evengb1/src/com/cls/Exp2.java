package com.cls;

import java.util.List;
import java.util.Stack;

public class Exp2 {
	
	public static void main(String[] args) {
		
		Stack data = new Stack();
		
		data.add(10);
		data.add("java");
		data.add(34.54);
		data.add(28.77f);
		data.add(false);
		data.add("wipro");
		data.add('A');
		data.add(10);
		data.add("java");
		
		System.out.println(data);
		
		System.out.println(data.peek());
		
		data.push("hello");
		
		System.out.println(data);
		
		System.out.println(data.peek());
		
		System.out.println(data.pop());
		
		System.out.println(data);
		
		System.out.println(data.search(100));
		
		System.out.println(data.empty());
		
		data.clear();
		
		System.out.println(data.empty());
	}

}
