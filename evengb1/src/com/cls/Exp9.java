package com.cls;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Exp9 {
	
	public static void main(String[] args) {
		
		List<String> names = new ArrayList<String>();
		
		names.add("lenovo");
		names.add("dell");
		names.add("sony");
		names.add("asus");
		names.add("apple");
		names.add("imac");
		names.add("dell");
		names.add("sony");
		
		
		 names.parallelStream().distinct().forEach(dt -> System.out.println(dt));
		 
		 System.out.println("==========");
		 
		 List<String> fltr = names.parallelStream().sorted().collect(Collectors.toList());
		 
		 System.out.println(fltr);
	} 

}
