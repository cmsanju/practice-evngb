package com.cls;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

class Customer implements Comparable<Customer>
{
	private int id;
	
	private String name;
	
	private int age;
	
	public Customer(int id, String name, int age)
	{
		this.id = id;
		this.name = name;
		this.age = age;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public int compareTo(Customer o) {
		
		return this.age - o.age;
	}
}

class NameComparator implements Comparator<Customer>
{

	@Override
	public int compare(Customer o1, Customer o2) {
		
		return o1.getName().compareTo(o2.getName());
	}
	
}

public class Exp8 {
	
	public static void main(String[] args) {
		
		List<Customer> list = new ArrayList<Customer>();
		
		list.add(new Customer(12, "lenovo", 23));
		list.add(new Customer(9, "java", 12));
		list.add(new Customer(6, "dell", 14));
		list.add(new Customer(5, "asus", 10));
		list.add(new Customer(2, "sony", 20));
		list.add(new Customer(3, "apple", 21));
		
		Collections.sort(list, new NameComparator());
		
		for(Customer dt : list)
		{
			System.out.println(dt.getId()+" "+dt.getName()+" "+dt.getAge());
		}
		
		list.forEach(dt -> System.out.println(dt.getId()+" "+dt.getName()+" "+dt.getAge()));
		
		//List<Customer> fltr = list.stream().filter(dt -> dt.getName().length() > 4).collect(Collectors.toList());
		
		//List<Customer> fltr = list.stream().filter(dt -> dt.getAge() > 19).collect(Collectors.toList());
		
		List<Customer> fltr = list.stream().filter(dt -> dt.getName().startsWith("a")).collect(Collectors.toList());
		
		System.out.println("===========");
		
		fltr.forEach(dt -> System.out.println(dt.getId()+" "+dt.getName()+" "+dt.getAge()));
		
		Map<String, Integer> data = list.stream().collect(Collectors.toMap(Customer::getName, Customer::getAge));
		
		System.out.println("============");
		
		data.forEach((x,y) -> System.out.println(x+" "+y));
	}

}
