package com.cls;

import java.util.ArrayList;
import java.util.List;

class Employee
{
	private int id;
	private String name;
	private String city;
	
	public Employee(int id, String name, String city)
	{
		this.id = id;
		this.name = name;
		this.city = city;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
}

public class Exp3 {
	
	public static void main(String[] args) {
		
		List<Employee> empList = new ArrayList<>();
		
		empList.add(new Employee(1, "Karthic", "blr"));
		
		empList.add(new Employee(2, "Karthic", "blr"));
		
		empList.add(new Employee(3, "Karthic", "blr"));
		
		empList.add(new Employee(4, "Karthic", "blr"));
		
		
		for(Employee dt : empList)
		{
			System.out.println(dt.getId()+" "+dt.getName()+" "+dt.getCity());
		}	
	}

}
