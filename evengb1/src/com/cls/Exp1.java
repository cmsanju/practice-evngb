package com.cls;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Vector;

public class Exp1 {
	
	public static void main(String[] args) {
		
		//List data = new ArrayList();
		
		//List data = new LinkedList();
		
		List data = new Vector();
		
		data.add(10);
		data.add("java");
		data.add(34.54);
		data.add(28.77f);
		data.add(false);
		data.add("wipro");
		data.add('A');
		data.add(10);
		data.add("java");
		
		System.out.println(data);
		
		System.out.println(data.size());
		
		System.out.println(data.contains(10));
		
		//Iterator , ListIterator and Enumeration 
		
		ListIterator ltr = data.listIterator();
		
		while(ltr.hasNext())
		{
			System.out.println(ltr.next());
		}
		
		System.out.println("=======");
		
		while(ltr.hasPrevious())
		{
			System.out.println(ltr.previous());
		}
		
		data.set(5, "hero");
		
		System.out.println(data);
		
		data.remove(6);
		
		System.out.println(data);
		
		
	}

}
/*
		
*/