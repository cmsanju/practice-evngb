package com.test;

import java.util.List;

public class Student {
	
	private int id;
	
	private String name;
	
	private List<String> skl;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getSkl() {
		return skl;
	}

	public void setSkl(List<String> skl) {
		this.skl = skl;
	}
	
	public void disp()
	{
		System.out.println(id+" "+name);
		
		for(String sk : skl)
		{
			System.out.println(sk);
		}
	}
}
