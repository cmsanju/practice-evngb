package com.test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;

public class EmpDao {
	
	private JdbcTemplate jdbcTemplate;

	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}
	
	public int save(Employee e)
	{
		String sql = "insert into emp11 values("+e.getId()+", '"+e.getName()+"', '"+e.getCmp()+"')";
		
		return jdbcTemplate.update(sql);
	}
	
	public int update(Employee e)
	{
		String sql = "update emp11 set emp_name = '"+e.getName()+"', cmp_name='"+e.getCmp()+"' where id="+e.getId()+"";
		
		return jdbcTemplate.update(sql);
	}
	
	public int delete(Employee e)
	{
		String sql = "delete from emp11 where id ="+e.getId()+" ";
		
		return jdbcTemplate.update(sql);
	}
	
	public List<Employee> readAll()
	{
		//String sql = "select id, emp_name, cmp_name from emp11";
		
		//List<Employee> list = new ArrayList<Employee>();
		
		
		return jdbcTemplate.query("select * from emp11", new ResultSetExtractor<List<Employee>>(){
			
			//@Override
			public List<Employee> extractData(ResultSet rs) throws SQLException, DataAccessException
			{
				
				List<Employee> list = new ArrayList<Employee>();
				
				while(rs.next())
				{
					Employee e = new Employee();
					
					e.setId(rs.getInt(1));
					e.setName(rs.getString(2));
					e.setCmp(rs.getString(3));
					
					list.add(e);
				}
				return list;
			}
		});
	}

}
