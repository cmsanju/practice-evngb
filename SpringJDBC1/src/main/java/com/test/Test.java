package com.test;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
	
	public static void main(String[] args) {
		
		ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
		
		EmpDao obj = (EmpDao)ctx.getBean("edao");
		
		Employee e = new Employee();
		
		e.setId(113);
		e.setName("apple");
		e.setCmp("CTS");
		
		obj.save(e);
		
		//obj.update(e);
		
		//obj.delete(e);
		
		List<Employee> emp = obj.readAll();
		
		for(Employee edata : emp)
		{
			System.out.println(edata.getId()+" "+edata.getName()+" "+edata.getCmp());
		}
			
		
		System.out.println("Done.");
	}

}
