package com.fls;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class DeserialDemo {
	
	public static void main(String[] args) throws Exception
	{
		
		FileInputStream fis = new FileInputStream("src/employee.txt");
		
		ObjectInputStream robj = new ObjectInputStream(fis);
		
		Employee obj = (Employee)robj.readObject();
		
		System.out.println(obj.id+" "+obj.name+" "+obj.city+" "+obj.pin);
	}

}
