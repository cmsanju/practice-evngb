package com.fls;

import java.io.File;
import java.io.FileOutputStream;

public class WriteData {
	
	public static void main(String[] args) throws Exception
	{
		
		File file = new File("src/write.txt");
		
		FileOutputStream fs = new FileOutputStream(file);
		
		String msg = "Hi this is byte stream read and write operations";
		
		fs.write(msg.getBytes());
		
		System.out.println("Done.");
	}

}
