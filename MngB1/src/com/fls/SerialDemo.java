package com.fls;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

public class SerialDemo {
	
	public static void main(String[] args) throws Exception
	{
		
		FileOutputStream fs = new FileOutputStream("src/employee.txt");
		
		ObjectOutputStream wr = new ObjectOutputStream(fs);
		
		Employee obj = new Employee();
		
		obj.id = 1212;
		obj.name = "Hero";
		obj.city = "Blr";
		obj.pin = 123123;
		
		wr.writeObject(obj);
		
		System.out.println("done");
	}

}
