package com.fls;

import java.io.FileWriter;

public class WriteTest {
	
	public static void main(String[] args) throws Exception
	{
		
		FileWriter fw = new FileWriter("src/sample.txt");
		
		String msg = "Hi this is Char stream file read and write operations";
		
		fw.write(msg);
		
		fw.flush();
		
		System.out.println("Done.");
	}

}
