package com.inh;

class G
{
	public void draw()
	{
		System.out.println("tle");
	}
}

class H extends G
{
	@Override
	public void draw()
	{
		System.out.println("rle");
	}
}

class I extends G
{
	@Override
	public void draw()
	{
		System.out.println("cle");
	}
}

public class Exp3 {
	
	public static void main(String[] args) {
		
		G h = new H();
		
		h.draw();
		
		G i = new I();
		
		i.draw();
	}

}
