package com.inh;

class A
{
	int id = 123;
	String name = "Java";
	
	public void details()
	{
		System.out.println("parent");
	}
}

class B extends A
{
	String city = "Blr";
	
	public void disp()
	{
		System.out.println(id+" "+name+" "+city);
	}
}


public class Exp1 {
	
	public static void main(String[] args) {
		
		B b = new B();
		
		b.details();
		b.disp();
		
	}

}
