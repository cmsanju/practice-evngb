package com.inh;

class C
{
	public void pet()
	{
		System.out.println("Top most parent");
	}
}

class D extends C
{
	public void dog()
	{
		System.out.println("Imt base class");
	}
}

class E extends D
{
	public void cat()
	{
		System.out.println("imt base class 2");
	}
}

class F extends E
{
	public void fox()
	{
		System.out.println("bottom most child class");
	}
}

public class Exp2 {
	
	public static void main(String[] args) {
		
		F f = new F();
		
		f.pet();
		f.dog();
		f.cat();
		f.fox();
	}

}
