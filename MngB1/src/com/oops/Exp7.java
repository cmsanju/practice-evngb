package com.oops;

abstract class Abs4
{
	public abstract void draw();
}

class Impl5 extends Abs4
{
	@Override
	public void draw()
	{
		System.out.println("tle");
	}
	
	public void book()
	{
		
	}
}

class Impl6 extends Abs4
{
	@Override
	public void draw() {
		
		System.out.println("cle");
	}
}

public class Exp7 {
	
	public static void main(String[] args) {
		
		
		Abs4 obj = new  Impl5();
		
		obj.draw();
		
		//obj.book();
		
		Abs4 obj1 = new Impl6();
		
		obj1.draw();

	}

}
