package com.oops;

abstract class Abs1
{
	public void pet()
	{
		System.out.println("implemented method from abs");
	}
	
	public abstract void animal();
}

class Impl4 extends Abs1
{

	@Override
	public void animal() {
		
		System.out.println("overriding abs method");
	}
	
}

public class Exp6 {
	
	public static void main(String[] args) {
		
		Impl4 obj = new Impl4();
		
		obj.animal();
		obj.pet();
		
	}

}
