package com.oops;

interface Inf2
{
	void pet();
	void color();
}

class Impl1 implements Inf2
{
	@Override
	public void pet()
	{
		System.out.println("dog");
	}
	
	@Override
	public void color()
	{
		System.out.println("green , skyblue");
	}
}

public class Exp2 {
	
	public static void main(String[] args) {
		
		Impl1 obj = new Impl1();
		
		obj.pet();
		obj.color();
		
	}

}
