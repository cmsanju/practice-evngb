package com.oops;

interface Inf1
{
	float pi = 3.14f;
	
	void pet();
	
	default void show()
	{
		System.out.println("default method");
	}
	
	static void disp()
	{
		System.out.println("static method");
	}
}

abstract class Abs
{
	
	public abstract void animal();
	
	public void details()
	{
		System.out.println("abs defautl");
	}
}

class Impl extends Abs implements Inf1
{
	public void animal()
	{
		System.out.println("abs overrided");
	}
	
	public void pet()
	{
		System.out.println("inf overrided");
	}
}

public class Exp1 {
	
	public static void main(String[] args) {
		
		Impl obj = new Impl();
		
		obj.animal();
		obj.pet();
		obj.details();
		obj.show();
		
		Inf1.disp();
	}

}
