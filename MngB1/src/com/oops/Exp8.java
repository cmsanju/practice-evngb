package com.oops;

interface Inf7
{
	void draw();
	
}

class Impl7 implements Inf7
{
	@Override
	public void draw() {
		
		System.out.println("tle");
	}
}

class Impl8  implements Inf7
{

	@Override
	public void draw() {
		
		System.out.println("cle");
	}
	
}

public class Exp8 {
	
	public static void main(String[] args) {
		
		Inf7 obj1 = new Impl7();
		
		obj1.draw();
		
		Inf7 obj2 = new Impl8();
		
		obj2.draw();
	}

}
