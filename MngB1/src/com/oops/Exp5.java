package com.oops;


interface Inf5
{
	String sayHello(int x, String name);
}

public class Exp5 {
	
	public static void main(String[] args) {
		
		Inf5 obj = new Inf5()
				{
			      public String sayHello(int x, String name)
			      {
			    	  return "Hi this is fun inf return statement";
			      }
				};
				
		Inf5 obj1 = (int x, String name) -> { System.out.println("overrided");
			
		 return "Test";
		};
		
	}

}

//java.io.Serializable, java.lang.Cloneable, java.rmi.Remote
