package com.oops;

interface Inf3
{
	void book();
	
	interface Inf4{
	
	void pens();
	
	}
}

class Impl3 implements Inf3.Inf4
{
	@Override
	public void pens()
	{
		System.out.println("pens method overrided");
	}
	
}

public class Exp3 {
	
	public static void main(String[] args) {
		
		Impl3 ojb = new Impl3();
		
		ojb.pens();
		
	}

}
