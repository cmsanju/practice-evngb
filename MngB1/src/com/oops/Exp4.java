package com.oops;

@FunctionalInterface
interface FunInf
{
	void greet();
}

public class Exp4 {
	
	public static void main(String[] args) {
		
		FunInf obj1 = new FunInf()
				{
					@Override
					public void greet()
					{
						System.out.println("overrided inf");
					}
				};
				
				obj1.greet();
				
		new FunInf()
		{
			@Override
			public void greet()
			{
				System.out.println("nameless object");
			}
		}.greet();
		
		//from jdk 8
		
		FunInf obj3 = () -> System.out.println("lambda expression");
		
		obj3.greet();
	}

}
