package com.oops;

import java.util.Scanner;

public class Test {
	
	//Student std = new Student();
	
	public static void main(String[] args) {
		
		Student std = new Student();
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("ENTER ID");
		
		int id = sc.nextInt();
		
		System.out.println("ENTER NAME");
		
		String name = sc.next();
		
		System.out.println("ENTER EMAIL");
		
		String email = sc.next();
		
		System.out.println("ENTER COLLEGE");
		
		String clg = sc.next();
		
		std.setId(id);
		std.setName(name);
		std.setEmail(email);
		std.setClg(clg);
		
		System.out.println(std.getId()+" "+std.getName()+" "+std.getEmail()+" "+std.getClg());
	}

}
