package com.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestExp5 {
	
	Exp5 obj = new Exp5();
	
	@Test
	public void testAreaOfRcle()
	{
		double rlt =obj.areaOfRecle(4.0, 6.0);
		
		assertEquals(24.0, rlt,0.001);
	}
	
	@Test
	public void testAreaOfCle()
	{
		double rlt = obj.areaOfCrcle(5.0);
		
		assertEquals(78.5, rlt,0.001);
	}

}
