package com.test;

public class Exp9 {
	
	public static void main(String[] args) {
		
		String str2 = "Hello";
		
		String str3 = "hello";
		
		System.out.println(str2 == str3);
		
		System.out.println(str2.equals(str3));
		
		System.out.println(str3.equalsIgnoreCase(str3));
		
		//how to convert immutable object into mutable
		
		StringBuffer sb = new StringBuffer(str2);
		
		System.out.println(sb.reverse());
		
		//reverse a string without using inbuilt methods
		
		for(int i = str2.length()-1; i >= 0; i--)
		{
			System.out.print(str2.charAt(i));
		}
	}

}
