package com.test;

public class Exp4 {
	
	static int x = 39;//class level data
	       int y = 30;//object level data
	 
	 public static void show()
	 {
		 System.out.println(Exp4.x);
		 
		 Exp4 e = new Exp4();
		 
		 System.out.println(e.y);
	 }
	 
	 public void disp()
	 {
		 System.out.println(y);
		 
		 System.out.println(Exp4.x);
	 }
	 
	 //static block
	 static
	 {
		 System.out.println("Satic block");
	 }
	 //instance block
	 {
		 System.out.println("instance block");
	 }
	 
	
	 public static void main(String[] args) {
		
		 Exp4.show();
		 
		 Exp4 obj = new Exp4();
		 
		 obj.disp();
	}

}
