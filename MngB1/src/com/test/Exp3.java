package com.test;

public class Exp3 {
	
	//type 1 method taking the arguments and returning the values
	
	public double method1(double x)
	{
		System.out.println("method 1");
		
		return x;
	}
	
	//type 2 method not taking the arguments not returning 
	
	public void method2()
	{
		System.out.println("method 2");
	}
	
	//type 3 method taking the arguments but not returning
	
	public void method3(String str)
	{
		System.out.println("method 3");
	}
	
	//type 4 method not taking the arguments but returning
	public String method4()
	{
		System.out.println("method 4");
		
		return "java";
	}
	
	public static void main(String[] args) {
		
		Exp3 x = new Exp3();
		
		x.method1(30.45);
		x.method2();
		x.method3("Hello");
		x.method4();
	}

}
