package com.test;

class Student
{
	public int id;//class variable
	public String name;
	
	public Student(int id, String name)// local variables
	{
		this.id = id;
		this.name = name;
	}
}

public class Exp7 {
	
	public static void main(String[] args) {
		
		Student[] ar = new Student[5];
		
		ar[0] = new Student(101, "user1");
		ar[1] = new Student(102, "user2");
		ar[2] = new Student(103, "user3");
		ar[3] = new Student(104, "user4");
		ar[4] = new Student(105, "user5");
		
		System.out.println(ar[0].id+" "+ar[0].name);
		
		for(int i = 0; i < ar.length; i++)
		{
			System.out.println(ar[i].id+" "+ar[i].name);
		}
		
		System.out.println("===========");
		
		for(Student data : ar)
		{
			System.out.println(data.id+" "+data.name);
		}
	}

}