package com.test;

public class Exp8 {
	
	public static void main(String[] args) {
		
		String str1 = "java";
		String str2 = "java";
		
		String str3 = new String("java");
		String str4 = new String("java");
		
		System.out.println(str3.hashCode());
		
		System.out.println(str4.hashCode());
		
		System.out.println(str1 == str2);
		
		System.out.println(str3 == str4);
		
		
		System.out.println(str3.equals(str4));
		
		System.out.println(str1.charAt(6));
	}

}
