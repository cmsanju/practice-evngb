package com.test;

public class Exp6 {
	
	public static void main(String[] args) {
		
		int[] ar = {12,34,56,78,90};
		
		//System.out.println(ar[0]);
		//System.out.println(ar[1]);
		
		
		for(int i = 0; i<ar.length; i++)
		{
			System.out.println(ar[i]);
		}
		
		System.out.println(ar.length);
		
		//System.out.println(ar[5]);
		
		for(int x : ar)
		{
			System.out.println(x);
		}
		
		String[] skills = {"java", "spring", "hiberante", "oracle"};
		
		for(String tech : skills)
		{
			System.out.println(tech);
		}
		
		int[] ar1 = new int[5];
		
		ar1[0] = 20;
		ar1[1] = 40;
		ar1[4] = 90;
		
		System.out.println(ar1[2]);
		
	}

}
