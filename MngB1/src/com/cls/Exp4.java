package com.cls;

import java.util.Iterator;
import java.util.TreeSet;

public class Exp4 {
	
	public static void main(String[] args) {
		
		TreeSet<Integer> data = new TreeSet<>();
		
		data.add(10);
		data.add(6);
		data.add(3);
		data.add(11);
		data.add(5);
		data.add(1);
		data.add(4);
		data.add(10);
		
		System.out.println(data);
		
		Iterator<Integer> itr = data.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
		
		
	}

}
