package com.cls;

import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public class Exp3 {
	
	public static void main(String[] args) {
		
		Set data = new HashSet();
		
		data.add(10);
		data.add("java");
		data.add(12.34f);
		data.add(34.90);
		data.add('S');
		data.add("java");
		data.add(false);
		data.add(10);
		
		System.out.println(data);
		
		Set dat = new LinkedHashSet();
		
		dat.add(10);
		dat.add("java");
		dat.add(12.34f);
		dat.add(34.90);
		dat.add('S');
		dat.add("java");
		dat.add(false);
		dat.add(10);
		
		System.out.println(dat);
		
		System.out.println(dat.size());
		
		System.out.println(dat.remove("java"));
		
		Iterator itr = dat.iterator();
		
		while(itr.hasNext())
		{
			System.out.println(itr.next());
		}
	}

}
