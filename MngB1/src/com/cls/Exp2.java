package com.cls;

import java.util.Stack;

public class Exp2 {
	
	public static void main(String[] args) {
		
		Stack data = new Stack();
		
		data.add(10);
		data.add("java");
		data.add(12.34f);
		data.add(34.90);
		data.add('S');
		data.add("java");
		data.add(false);
		data.add(10);
		data.add("hello");
		
		System.out.println(data);
		
		System.out.println(data.peek());
		
		data.push("king");
		
		System.out.println(data.peek());
		
		System.out.println(data.pop());
		
		System.out.println(data);
		
		System.out.println(data.search("javarq"));
		
		System.out.println(data.empty());
		
		data.clear();
		
		System.out.println(data.empty());
	}

}
