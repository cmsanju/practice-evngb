package com.cls;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Vector;

public class Exp1 {
	
	public static void main(String[] args) {
		
		//List data = new ArrayList();
		
		//List data = new LinkedList();
		
		List data = new Vector();
		
		data.add(10);
		data.add("java");
		data.add(12.34f);
		data.add(34.90);
		data.add('S');
		data.add("java");
		data.add(false);
		data.add(10);
		
		System.out.println(data);
		
		System.out.println(data.size());
		
		data.add("hello");
		System.out.println(data.size());
		
		data.set(5, "dell");
		
		data.remove(6);
		
		System.out.println(data);
		
		System.out.println(data.contains(100));
		
		//Iterator, ListIterator and Enumeration 
		
		//Iterator itr = data.iterator();
		
		ListIterator ltr = data.listIterator();
		
		while(ltr.hasNext())
		{
			System.out.println(ltr.next());
		}
		
		System.out.println("======");
		
		while(ltr.hasPrevious())
		{
			System.out.println(ltr.previous());
		}
	}

}
