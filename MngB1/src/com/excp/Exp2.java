package com.excp;

public class Exp2 {
	
	public static void main(String[] args) {
		
		try
		{
			System.out.println(10/2);
			
			String st = "abc";
			
			int x = Integer.parseInt(st);
		}
		catch(Exception e)
		{
			//1 only exception message getMessage()
			
			System.out.println(e.getMessage());
			
			//2 printing exception class object it will give exception class name and message
			
			System.out.println(e);
			
			//3 printStackTrace() it will give exception class name, message and line number
			
			e.printStackTrace();
		}
	}

}
