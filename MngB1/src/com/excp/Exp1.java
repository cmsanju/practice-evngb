package com.excp;

public class Exp1 {
	
	public static void main(String[] args) {
		
		try
		{
			//100 lines
			System.out.println("test");
			System.out.println(10/0);
			
			int[] ar = {12,34,56};
			
			System.out.println(ar[2]);
			
			String str = "java";
			
			System.out.println(str.charAt(2));
			
			String tr = null;
			
			//System.out.println(tr.charAt());
			
			String st = "abc";
			
			int x = Integer.parseInt(st);
		}
		
		catch(ArithmeticException ae)
		{
			System.out.println("can't divided by zero");
		}
		catch (ArrayIndexOutOfBoundsException aie) {
			
			System.out.println("check your array size");
		}
		catch(StringIndexOutOfBoundsException sie)
		{
			System.out.println("check your string length");
		}
		catch(NullPointerException npe)
		{
			System.out.println("please enter string data");
		}
		catch(Exception e)
		{
			System.out.println("check your inputs");
		}
	
		finally
		{
			System.out.println("i am from finally");
			
			try {
				
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		
	}

}
